import Vue from 'vue'
import App from './App'

Vue.config.productionTip = false


uni.getSystemInfo({
	success(res) {
		Vue.prototype.$system = res
		uni.setStorage({
			key: "$system",
			data: res
		})
	}
});
Vue.prototype.websiteUrl = 'http://uniapp.dcloud.io';


uni.setStorage({
	key: "storage_key",
	data: {}
})

App.mpType = 'app'

const app = new Vue({
	...App
})
app.$mount()
