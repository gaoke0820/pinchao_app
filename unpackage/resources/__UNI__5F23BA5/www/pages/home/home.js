// { "framework": "Vue"} 

/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "C:\\Users\\Administrator\\Documents\\HBuilderProjects\\品潮\\pages\\home\\home.nvue?entry");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./node_modules/weex-vue-loader/lib/script-loader.js!./node_modules/babel-loader/lib/index.js!./node_modules/weex-vue-loader/lib/selector.js?type=script&index=0!C:\\Users\\Administrator\\Documents\\HBuilderProjects\\品潮\\pages\\home\\home.nvue":
/*!****************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/weex-vue-loader/lib/script-loader.js!./node_modules/babel-loader/lib!./node_modules/weex-vue-loader/lib/selector.js?type=script&index=0!C:/Users/Administrator/Documents/HBuilderProjects/品潮/pages/home/home.nvue ***!
  \****************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });exports.default = void 0; //
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
var _default =
{};exports.default = _default;

/***/ }),

/***/ "./node_modules/weex-vue-loader/lib/style-loader.js!./node_modules/weex-vue-loader/lib/style-rewriter.js?id=data-v-0179feb8!./node_modules/weex-vue-loader/lib/selector.js?type=styles&index=0!C:\\Users\\Administrator\\Documents\\HBuilderProjects\\品潮\\pages\\home\\home.nvue":
/*!*******************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/weex-vue-loader/lib/style-loader.js!./node_modules/weex-vue-loader/lib/style-rewriter.js?id=data-v-0179feb8!./node_modules/weex-vue-loader/lib/selector.js?type=styles&index=0!C:/Users/Administrator/Documents/HBuilderProjects/品潮/pages/home/home.nvue ***!
  \*******************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = {
  "status_bar": {
    "width": 750,
    "height": 44,
    "backgroundColor": "#000000"
  },
  "searchbox": {
    "width": 750,
    "height": 84,
    "backgroundColor": "#000000"
  },
  "s_input": {
    "width": 702,
    "height": 60,
    "marginTop": 12,
    "marginRight": 24,
    "marginBottom": 12,
    "marginLeft": 24,
    "backgroundColor": "#ffffff",
    "textAlign": "center"
  },
  "scroller": {
    "width": 750,
    "height": 74,
    "borderBottomWidth": 1,
    "borderBottomStyle": "solid",
    "borderBottomColor": "#E6E6E6",
    "flexDirection": "row"
  },
  "scrollItem": {
    "width": 80,
    "height": 74,
    "marginTop": 0,
    "marginRight": 20,
    "marginBottom": 0,
    "marginLeft": 20,
    "textAlign": "center",
    "lineHeight": 74,
    "display": "inline-block",
    "fontSize": 32
  },
  "curItem": {
    "borderBottomWidth": 4,
    "borderBottomColor": "#DBC600"
  }
}

/***/ }),

/***/ "./node_modules/weex-vue-loader/lib/template-compiler.js?id=data-v-0179feb8!./node_modules/weex-vue-loader/lib/selector.js?type=template&index=0!C:\\Users\\Administrator\\Documents\\HBuilderProjects\\品潮\\pages\\home\\home.nvue":
/*!*********************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/weex-vue-loader/lib/template-compiler.js?id=data-v-0179feb8!./node_modules/weex-vue-loader/lib/selector.js?type=template&index=0!C:/Users/Administrator/Documents/HBuilderProjects/品潮/pages/home/home.nvue ***!
  \*********************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _vm._m(0)
},staticRenderFns: [function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', [_c('div', {
    staticClass: ["status_bar"]
  }, [_c('div', {
    staticClass: ["top_div"]
  })]), _c('div', {
    staticClass: ["content"]
  }, [_c('div', {
    staticClass: ["searchbox"]
  }, [_c('input', {
    staticClass: ["s_input"],
    attrs: {
      "type": "text",
      "placeholder": "搜索",
      "value": ""
    }
  })]), _c('scroller', {
    staticClass: ["scroller"],
    attrs: {
      "scrollDirection": "horizontal"
    }
  }, [_c('text', {
    staticClass: ["scrollItem", "curItem"]
  }, [_vm._v(" 推荐 ")]), _c('text', {
    staticClass: ["scrollItem"]
  }, [_vm._v(" 附近 ")]), _c('text', {
    staticClass: ["scrollItem"]
  }, [_vm._v(" 南京 ")]), _c('text', {
    staticClass: ["scrollItem"]
  }, [_vm._v(" 北京 ")]), _c('text', {
    staticClass: ["scrollItem"]
  }, [_vm._v(" 上海 ")]), _c('text', {
    staticClass: ["scrollItem"]
  }, [_vm._v(" 广州 ")]), _c('text', {
    staticClass: ["scrollItem"]
  }, [_vm._v(" 深圳 ")]), _c('text', {
    staticClass: ["scrollItem"]
  }, [_vm._v(" 北京 ")]), _c('text', {
    staticClass: ["scrollItem"]
  }, [_vm._v(" 上海 ")]), _c('text', {
    staticClass: ["scrollItem"]
  }, [_vm._v(" 广州 ")]), _c('text', {
    staticClass: ["scrollItem"]
  }, [_vm._v(" 深圳 ")])])])])
}]}

/***/ }),

/***/ "C:\\Users\\Administrator\\Documents\\HBuilderProjects\\品潮\\pages\\home\\home.nvue?entry":
/*!***************************************************************************************!*\
  !*** C:/Users/Administrator/Documents/HBuilderProjects/品潮/pages/home/home.nvue?entry ***!
  \***************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var __vue_exports__, __vue_options__
var __vue_styles__ = []

/* styles */
__vue_styles__.push(__webpack_require__(/*! !./node_modules/weex-vue-loader/lib/style-loader!./node_modules/weex-vue-loader/lib/style-rewriter?id=data-v-0179feb8!./node_modules/weex-vue-loader/lib/selector?type=styles&index=0!./home.nvue */ "./node_modules/weex-vue-loader/lib/style-loader.js!./node_modules/weex-vue-loader/lib/style-rewriter.js?id=data-v-0179feb8!./node_modules/weex-vue-loader/lib/selector.js?type=styles&index=0!C:\\Users\\Administrator\\Documents\\HBuilderProjects\\品潮\\pages\\home\\home.nvue")
)

/* script */
__vue_exports__ = __webpack_require__(/*! !./node_modules/weex-vue-loader/lib/script-loader!babel-loader!./node_modules/weex-vue-loader/lib/selector?type=script&index=0!./home.nvue */ "./node_modules/weex-vue-loader/lib/script-loader.js!./node_modules/babel-loader/lib/index.js!./node_modules/weex-vue-loader/lib/selector.js?type=script&index=0!C:\\Users\\Administrator\\Documents\\HBuilderProjects\\品潮\\pages\\home\\home.nvue")

/* template */
var __vue_template__ = __webpack_require__(/*! !./node_modules/weex-vue-loader/lib/template-compiler?id=data-v-0179feb8!./node_modules/weex-vue-loader/lib/selector?type=template&index=0!./home.nvue */ "./node_modules/weex-vue-loader/lib/template-compiler.js?id=data-v-0179feb8!./node_modules/weex-vue-loader/lib/selector.js?type=template&index=0!C:\\Users\\Administrator\\Documents\\HBuilderProjects\\品潮\\pages\\home\\home.nvue")
__vue_options__ = __vue_exports__ = __vue_exports__ || {}
if (
  typeof __vue_exports__.default === "object" ||
  typeof __vue_exports__.default === "function"
) {
__vue_options__ = __vue_exports__ = __vue_exports__.default
}
if (typeof __vue_options__ === "function") {
  __vue_options__ = __vue_options__.options
}

__vue_options__.render = __vue_template__.render
__vue_options__.staticRenderFns = __vue_template__.staticRenderFns
__vue_options__._scopeId = "data-v-0179feb8"
__vue_options__.style = __vue_options__.style || {}
__vue_styles__.forEach(function (module) {
  for (var name in module) {
    __vue_options__.style[name] = module[name]
  }
})
if (typeof __register_static_styles__ === "function") {
  __register_static_styles__(__vue_options__._scopeId, __vue_styles__)
}

module.exports = __vue_exports__
module.exports.el = 'true'
new Vue(module.exports)


/***/ })

/******/ });